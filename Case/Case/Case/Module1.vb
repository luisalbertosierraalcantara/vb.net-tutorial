﻿Module Module1

    Sub Main()
        Dim x As Integer = 1

        Select Case x
            Case 1
                Console.WriteLine("Selected: {0}", "1")
            Case 2
                Console.WriteLine("Selected: {0}", "2")
            Case Else
                Console.WriteLine("Selected: {0}", "3")
        End Select

        Console.ReadLine()
    End Sub

End Module
