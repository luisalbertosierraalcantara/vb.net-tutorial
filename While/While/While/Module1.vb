﻿Module Module1

    Sub Main()
        Dim i As Integer = 1
        Dim condition As Boolean = True

        While (condition)

            If i = 50 Then condition = False

            Console.WriteLine("Number: {0}", CStr(i))

            i = i + 1

        End While

        Console.ReadLine()
    End Sub

End Module
