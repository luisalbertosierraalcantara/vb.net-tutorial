﻿Module Module1

    Sub Main()
        Console.WriteLine("Convert to DateTime")
        Console.WriteLine("-----------------------------------")
        Dim text As String = "13/10/2022 12:00"
        Dim _datetime As Date
        _datetime = CDate(text)
        Console.WriteLine("DateTime: {0}", CStr(_datetime))
        Console.WriteLine(Chr(13))
        Console.ReadLine()
    End Sub

End Module
