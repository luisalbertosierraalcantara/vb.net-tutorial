﻿Module Module1

    Sub Main()
        Dim phones As String() ' Arrays of strings
        phones = {"1809-445-5664", "1983-345-3333"} ' assingnment  the array

        Dim names() As String ' Arrays of strings
        names = {"Luis A. Sierra", "Jose Bancuver"} ' assingnment  the array

        Dim car(3) As String ' Arrays of three strings
        car(0) = "Ford" ' assingnment the index 0 of the Array

        Dim Colors() As String = {"Red", "Blue", "White"} ' definition and assingnment at the same time

        ' print in console the index 0

        Console.WriteLine("Array phone: {0}", phones(0))
        Console.WriteLine("Array name: {0}", names(0))
        Console.WriteLine("Array car: {0}", car(0))
        Console.WriteLine("Array Color: {0}", Colors(0))
        Console.ReadLine()

    End Sub

End Module
