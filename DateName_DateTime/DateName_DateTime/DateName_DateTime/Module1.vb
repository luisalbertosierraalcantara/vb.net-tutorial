﻿Module Module1

    Sub Main()
        Console.WriteLine("DayName of DateTime")
        Console.WriteLine("-----------------------------------")
        Dim DayName As String
        Dim text As String = "13/10/2022 12:00"
        Dim date_time As Date
        date_time = CDate(text)
        Dim DayNumber As Integer = date_time.DayOfWeek()

        If DayNumber = 1 Then
            DayName = "Sunday"
        ElseIf DayNumber = 2 Then
            DayName = "Monday"
        ElseIf DayNumber = 3 Then
            DayName = "Tuesday"
        ElseIf DayNumber = 4 Then
            DayName = "Wednesday"
        ElseIf DayNumber = 5 Then
            DayName = "Thursday"
        ElseIf DayNumber = 6 Then
            DayName = "Friday"
        ElseIf DayNumber = 7 Then
            DayName = "Saturday"
        End If

        Console.WriteLine("Day: {0}", DayName)
        Console.WriteLine(Chr(13))
        Console.ReadLine()
    End Sub

End Module
