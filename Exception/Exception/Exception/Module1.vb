﻿Module Module1

    Sub Main()

        Try
            Dim result As Integer
            Dim x, y As String

            Console.Write("Enter the First Number: ")
            x = Console.ReadLine

            Console.Write("Enter the Second Number: ")
            y = Console.ReadLine

            result = CInt(x) / CInt(x)

            Console.WriteLine("result: {0}", CStr(result))

        Catch e As DivideByZeroException
            Console.WriteLine("result: {0}", e.ToString)
        Finally
            ' Always stop by here
        End Try

        Console.ReadLine()

    End Sub

End Module
