﻿Public Class Class2

    Inherits Class1 'Inherits Property from Class 1

    Public Sub ShowData()

        Pro_Account = 357886 'Set Property to Class 1

        'Print data to console
        Console.WriteLine("Account: {0}", CStr(Pro_Account)) 'Get Property from Class 1
        Console.WriteLine("Name: Luis A. Sierra")
    End Sub

End Class
