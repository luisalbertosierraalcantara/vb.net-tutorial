﻿Module Module1

    Sub Main()
        Dim names(5) As String
        Dim name As String

        names(0) = "Luis A. Sierra"
        names(1) = "Mattew"
        names(2) = "Victoria"
        names(3) = "Josue"
        names(4) = "Kelvin"
        names(5) = "Victor"

        For Each name In names
            Console.WriteLine("Name: {0}", name)
        Next

        Console.ReadLine()
    End Sub

End Module
