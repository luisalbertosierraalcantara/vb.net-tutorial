﻿Module Module1

    Sub Main()
        Console.WriteLine("Convert to double")
        Console.WriteLine("-----------------------------------")
        Dim text As String = "19.5"
        Dim number As Double = CDbl(text)
        Console.WriteLine("Double Decimal: {0}", CStr(number))
        Console.WriteLine(Chr(13))
        Console.ReadLine()
    End Sub

End Module
