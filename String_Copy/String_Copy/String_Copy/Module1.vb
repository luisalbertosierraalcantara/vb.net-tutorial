﻿Module Module1

    Sub Main()
        Console.WriteLine("String Copy")
        Console.WriteLine("-----------------------------------")
        Dim text As String
        text = "Basic"
        Console.WriteLine(text)

        Dim chrs(5) As Char
        text.CopyTo(0, chrs, 0, 5)
        Console.WriteLine("Copy: {0}", chrs(3))

        Console.WriteLine(Chr(13))
        Console.ReadLine()
    End Sub

End Module
