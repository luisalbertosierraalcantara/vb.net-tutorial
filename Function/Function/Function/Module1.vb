﻿Module Module1

    Public Function SUM(ByVal x As Integer, ByVal y As Integer) As Integer
        Dim result As Integer

        result = x + y

        Return result
    End Function

    Sub Main()
        Console.WriteLine("SUM: {0}", CStr(SUM(5, 5)))
        Console.ReadLine()
    End Sub

End Module
