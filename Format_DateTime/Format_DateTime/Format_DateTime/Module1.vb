﻿Module Module1

    Sub Main()
        Console.WriteLine("Format DateTime")
        Console.WriteLine("-----------------------------------")
        Dim text As String = "13/10/2022 12:00"
        Dim _datetime As Date
        _datetime = CDate(text)
        Console.WriteLine("Format: {0}", CStr(FormatDateTime(_datetime, DateFormat.LongDate)))
        Console.WriteLine(Chr(13))
        Console.ReadLine()
    End Sub

End Module
