﻿Module Module1

    Sub SUM(ByVal x As Integer, ByVal y As Integer)
        Dim result As Integer

        result = x + y

        Console.WriteLine("SUM: {0}", CStr(result))
    End Sub

    Sub Main()
        SUM(5, 5)
        Console.ReadLine()
    End Sub

End Module
