﻿Module Module1

    Sub Main()
        Console.WriteLine("Convert to String")
        Console.WriteLine("-----------------------------------")
        Console.WriteLine("String Date: {0}", CStr(Date.Now))
        Console.WriteLine("String Number: {0}", CStr(67))
        Console.WriteLine("String Decimal: {0}", CStr(96.89))
        Console.WriteLine("String Boolean: {0}", CStr(True))
        Console.WriteLine(Chr(13))
        Console.ReadLine()
    End Sub

End Module
